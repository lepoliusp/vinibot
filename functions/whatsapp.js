const functions = require('firebase-functions');
const db = require('./database.js');

const twilio = require('twilio')
const accountSid = functions.config().twilio.sid;
const authToken = functions.config().twilio.token;
const client = twilio(accountSid, authToken);

// Twilio WhatsApp Sandbox. Manage yours at https://www.twilio.com/console/sms/whatsapp/sandbox.
const sandbox = 'whatsapp:+14155238886'

exports.inbox = functions.https
  .onRequest(async (req, res) => {
    const wpp = req.body.From.replace('whatsapp:', '');
    const twiml = new twilio.twiml.MessagingResponse();

    try {
      const name = await db.queryUserName(wpp);
      if (name === null)
        twiml.message('Não localizamos seu perfil. Acesse https://lepoli-usp.web.app/ para se cadastrar.');
      else twiml.message(`Olá, ${name}`);
    } catch (err) {
      console.err('Error getting documents', err);
    }

    res.writeHead(200, {'Content-Type': 'text/xml'});
    res.end(twiml.toString());
  });

exports.statusCallback = functions.https
  .onRequest(async (req, res) => {
    const messageSid = req.body.MessageSid;
    const messageStatus = req.body.MessageStatus;
    console.info(`SID: ${messageSid}, Status: ${messageStatus}`);
  });

exports.triggerSale = functions.firestore
  .document('sales/{saleId}')
  .onCreate(async (snap, context) => {
    // Get key values from document path.
    console.info(context.params);
    
    // Get an object representing the document.
    const newSale= snap.data();    
    console.info(newSale);

    var textMessage = {
      body: `Aqui está: ${newSale.good} ${newSale.brand} por apenas *R$${newSale.price}* reais.`,
      from: sandbox,
    }
    
    switch(newSale.price) {
      case "7,50":
        textMessage.mediaUrl =  ['https://i.imgur.com/jxpDAR9.jpg'];
        break;
      case "14,90":
        textMessage.mediaUrl =  ['https://i.imgur.com/Niuhkbk.jpg'];
        break;
      case "9,99":
        textMessage.mediaUrl =  ['https://i.imgur.com/KS3Fxmr.jpg'];
        break;
      default:
        break;
    }
    
    const arr = await db.queryUsers();
    console.info(arr);

    var index = arr.indexOf(newSale.owner); 
    if (index > -1)
       arr.splice(index, 1);

    arr.forEach(async wpp => {
      setTimeout(() => {  console.log("Delay"); }, 2000);
      textMessage.to = `whatsapp:${wpp}`;
      let message = await client.messages.create(textMessage);
      console.log(message.sid);
    })
  });