const functions = require('firebase-functions'),
      validator = require("validator"),
      db = require('./database.js');

exports.greetings = functions.https
  .onRequest(async(req, res) => {
    console.info(req.body);
    const wpp = req.body.UserIdentifier.replace('whatsapp:', '');
    const name = await db.queryUserName(wpp);

    const object = name === null ?
      { 
        actions:
        [
          {
            say: "Não localizamos seu perfil. Acesse https://lepoli-usp.web.app/ para se cadastrar."
          }
        ]
      } : {
            actions:
            [
              {
                say: `Olá, ${name}! Em que posso ajudá-lo?`
              },
              {
                remember:
                {
                  first_name: name,
                  whatsapp: wpp
                }
              },
              {
                listen: true
              }
            ]
          };
          
    res
      .status(200)
      .json(object)
      .end();
  });

exports.confirmSale = functions.https
  .onRequest(async(req, res) => {
    console.info(req.body);
    const memory = JSON.parse(req.body.Memory);
    const answers = memory.twilio.collected_data.sale.answers;
    var newSale = {
      name: answers.name.answer,
      brand: answers.brand.answer,
      price: req.body.CurrentInput
    }

    newSale.price = validator.toFloat(newSale.price.replace(',', '.'));
    newSale.price = newSale.price.toFixed(2).replace('.', ',');

    const object =
      {
        actions:
        [
          {
            collect:
            {
              name: "confirmation",
              questions:
              [
                {
                  question: `Promoção de *${newSale.name}* da marca *${newSale.brand}* por *R$ ${newSale.price}*. Confirma?`,
                  name: "confirmation",
                  type: "Boolean",
                }
              ],
              on_complete: { redirect: "https://us-central1-lepoli-usp.cloudfunctions.net/nlu-addSale" }
            }
          },
          {
            remember:
            {
              sale_name: newSale.name,
              sale_brand: newSale.brand,
              sale_price: newSale.price
            }
          }
        ]
      };

    res
      .status(200)
      .json(object)
      .end();
  });

exports.validatePrice = functions.https
  .onRequest(async(req, res) => {   
    var price = req.body.CurrentInput;
    var valid = validator.isFloat(price.replace(',', '.'));
    res
      .status(200)
      .json({"valid": valid})
      .end();
});

exports.addSale = functions.https
  .onRequest(async(req, res) => {
    const memory = JSON.parse(req.body.Memory);
    console.info(memory);
    const confirmation = memory.twilio.collected_data.confirmation.answers.confirmation.answer.toUpperCase();

    if (confirmation === 'NÃO') {
      res
        .status(200)
        .json(restartTask('new_sale'))
        .end();
      return;
    }

    const object = {
      actions:
      [
        {
          say: 'Certo! Iremos disparar a promoção para seus clientes fidelizados.'
        },
        {
          redirect: `task://new_sale_more`     
        }
      ]
    };

    const docRef = await db.addSale({
      good: memory.sale_name,
      brand: memory.sale_brand,
      price: memory.sale_price, // TODO converter em número
      owner: memory.whatsapp
    });

    res
      .status(200)
      .json(object)
      .end();
  });

exports.validateMore = functions.https
  .onRequest(async(req, res) => {
    const memory = JSON.parse(req.body.Memory);
    console.info(memory);
    const confirmation = memory.twilio.collected_data.confirmation.answers.confirmation.answer.toUpperCase();

    if (confirmation === 'NÃO') {
      res
        .status(200)
        .json({
          actions:
            [
              {
                say: 'Ok! Até mais.'     
              }
            ]
        })
        .end();
      return;
    }

    const object = {
      actions:
      [
        {
          redirect: 'task://new_sale'
        }
      ]
    };

    res
      .status(200)
      .json(object)
      .end();
  });

exports.breakdown = functions.https
  .onRequest(async(req, res) => {
    const memory = JSON.parse(req.body.Memory);

    const object = {
      actions: [
        {
          say: `${memory.first_name}, pedimos desculpas pelo transtorno. Agradeço recomeçar por aqui ou.... form, atendente, mandar áudio, etc`
        }
      ]
    }

    res
      .status(200)
      .json(object)
      .end();
  });

function restartTask(task_name) {
    return {
      actions:
      [
        {
          say: 'Ok! Vamos tentar novamente.'
        },
        {
          redirect: `task://${task_name}`     
        }
      ]
    }
  }