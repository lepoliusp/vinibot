// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Firestore Database.
const admin = require('firebase-admin');
admin.initializeApp();

// Reference to our Firebase Firestore Database.
const db = admin.firestore();

// This library is used to format phone numbers.
// More information at https://www.npmjs.com/package/google-libphonenumber.
const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();
const PNF = require('google-libphonenumber').PhoneNumberFormat;

// Callback from own web SIGN UP page.
// Html form is available from https://lepoli-usp.web.app.
exports.addUser = functions.https
  .onCall(async (data, context) => {
    console.log(data);


    // Check if recaptcha was filled.
    if (data.captcha === undefined || data.captcha === '' || data.captcha === null)
      return { message: 'Captcha não preenchido. Preencha e tente novamente.' };

    // Check if recaptcha is valid.
    var result = await validateCaptcha('6LcH8_EUAAAAAKdN6Ar-kzL6NXJQG3zxsL9hTkkT', data.captcha);
    if (!result.success)
      return { message: 'A verificação de segurança falhou. Você é um robô?' };

    // Parse number with country code and keep raw input.
    const number = phoneUtil.parseAndKeepRawInput(data.wpp, 'BR');
    
    // Validate inserted phone number.
    if (!phoneUtil.isValidNumberForRegion(number, 'BR'))
      return { message: 'O número de telefone inserido não é válido. Verifique e tente novamente.' }

    const e164 = phoneUtil.format(number, PNF.E164);
    
    // Check if phone number is already in use.
    if (await this.queryUserName(e164) !== null)
      return {
        message: `Esse número de telefone já foi cadastrado (${e164}).`
      }

    // Finally create a new user.
    const docRef = await db.collection('users').add({
      name: data.name,
      whatsapp: e164
    });

    return {
      id: docRef.id,
      message: 'Sucesso! Seu usuário foi cadastrado.'
    };
  });

exports.addSale = async(data) => {
    db.collection('sales')
      .add(data);
  };

exports.queryUserName = async(phoneNumber) => {
    let usersRef = db.collection('users');
    let querySnapshot = await usersRef.where('whatsapp', '==', phoneNumber).get();
  
    if (querySnapshot.empty)
      return null;
    return querySnapshot.docs[0].get('name');
  }

exports.queryUsers = async() => {
  var querySnapshot = await db.collection("users").get()
  var result = [];

  querySnapshot.forEach(doc => {
    let whatsapp = doc.get('whatsapp');
    result.push(whatsapp);
  });
  
  return result;
};

function validateCaptcha(secret, response) {
  return require('request-promise')({
    uri: 'https://recaptcha.google.com/recaptcha/api/siteverify',
    method: 'POST',
    formData: {
        'secret': secret,
        'response': response
    },
    json: true
  });
}