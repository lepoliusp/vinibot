# ViniBot 
---

Uma pesquisa de 2015 do SEBRAE comprova que a maior dificuldade dos minimercados não é a entrega dos produtos em domicílio, já que esse é o serviço mais oferecidos pelos minimercados do Brasil (59,4% na época); mas sim a comunicação com os clientes e a publicidade nesse tradicional setor de varejo. Pensando nisso, criamos o ViniBot!

Ele é um chatbot pelo qual, em uma conversa de WhatsApp, os donos de minimercado conseguem cadastrar promoções e descontos, que são então enviados para os clientes cadastrados. A ideia é implementar divulgação viral entre pessoas da região, que irão compartilhar as fotos de promoções uns com os outros.

## Fluxograma básico do MVP
---

![Fluxograma básico do MVP](https://i.imgur.com/99w2opt.jpg)

## APIs
---
As APIs da Twilio foram utilizadas de maneira integrada ao Firebase. São elas:

### Twilio
- Autopilot
- Programmable SMS

### Firebase
- Cloud Firestore 
- Cloud Functions
- Hosting

## GR1D
---
Infelizmente, em função do tempo empregado no estudo da Twilio, não conseguimos reunir esforços para também aprender e começar a usar as APIs disponibilizadas pela GR1D. Entretanto, algumas APIs do Marketplace oferecem serviços extremamente atrativos para o nosso negócio, como a [REBEL](https://finance.gr1d.io/developers/api/rebel-api-de-acesso-externo/9d3055c1-1c64-4107-b055-c11c6431079b), e as APIs de emissão de NFe, como a [WebmaniaBR NFe](https://finance.gr1d.io/developers/api/undefined/8c83596e-54c4-449c-8359-6e54c4149c6a) e a [Focus NFe](https://finance.gr1d.io/developers/api/undefined/14fbac4e-a87c-44cb-bbac-4ea87c44cbd5). Ou seja, ao integrarmos tais APIs à solução inicialmente proposta e entregue, seria possível, por exemplo, a emissão e o envio de notas fiscais direto pelo WhatsApp, integração de outras etapas da cadeia B2C ao meio digital, pagamento direto pelo WhatsApp, bem como outras funcionalidades. Inclusive, permitir que o empreendedor consulte empréstimos na Rebel direto pelo WhatsApp! Para esse sonho tornar-se realidade um dia, só precisamos começar!